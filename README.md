# Jai-ImGui

This repository contains the Docking branch of [ImGui](https://github.com/ocornut/imgui/tree/docking) updated to its latest version.
So it's not the same thing as the module that's shipped with Jai.

For now, I just compiled it on Windows, but it should be working on Linux and MacOS.

This repo uses Git submodules, so clone with :

```
git clone https://gitlab.com/Stowy/jai-imgui.git --recursive
```