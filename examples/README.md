# Jai-ImGui Examples

This folder contains examples of using Jai-ImGui.

-   `imgui_sdl_gl.jai`: This has a rendering function for OpenGL. You can't run this file.
-   `imgui_sdl_gl_main.jai`: This is a program that can be used to test the function in the file above.
-   `imgui_inspector.jai`: Demonstration of some of the reflection features of Jai to build an inspector with ImGui.

To be able to run these on Windows, you need to copy `SDL2.dll` in this folder. You can find it in `<jai-root>\modules\SDL\windows\`.
